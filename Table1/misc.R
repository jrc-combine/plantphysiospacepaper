### Functions to make the ground truth for benchmarking PhysioSpace
##what they do is they just calculate fold-change of each sample (except 
#controls of course) from the same database that I made Spaces before
## by esfahani@aices.rwth-aachen.de

GroupFoldChangeMaker <- function(GSE, Annot, isRNASeq = FALSE){
  SimpletonIndices <- which(paste0(Annot$Curated_Control[-nrow(Annot)],
                                   Annot$Curated_Control[-1]) == "TC")
  if(length(SimpletonIndices)==0){
    return(SimpletonFoldChangeMaker(GSE, Annot, isRNASeq))
  } else {
    Indx <- 1
    while(Indx < max(SimpletonIndices)){
      SubAnnotIndices <- Indx:min(SimpletonIndices[Indx<SimpletonIndices])
      if(length(SubAnnotIndices)<2) stop("Problem in indexing")
      AnnotSub <- Annot[SubAnnotIndices,]
      if(exists("OutMat")){
        OutMat <- cbind(OutMat,SimpletonFoldChangeMaker(GSE, AnnotSub, isRNASeq))
      } else {
        OutMat <- SimpletonFoldChangeMaker(GSE, AnnotSub, isRNASeq)
      }
      Indx <- max(SubAnnotIndices) + 1
    }
    SubAnnotIndices <- Indx:nrow(Annot)
    if(length(SubAnnotIndices)<2) stop("Problem in indexing")
    AnnotSub <- Annot[SubAnnotIndices,]
    OutMat <- cbind(OutMat, SimpletonFoldChangeMaker(GSE, AnnotSub, isRNASeq))
    return(OutMat)
  }
}

SimpletonFoldChangeMaker <- function(GSE, Annot, isRNASeq){
  
  if(is.character(GSE)){
    message("Making a simpleton space from ", GSE)
    GSEexpr <- readRDS(paste0("Table1/ExprSets/",GSE,".rds"))
  } else {
    GSEexpr <- GSE
  }
  
  if(isRNASeq){
    if(any(GSEexpr<0)) stop("I have a problem here!")
    if(!is.integer(GSEexpr[1,1])) stop("I have a problem here too!")
    GSEexpr[GSEexpr==0] <- 1
    GSEexpr <- log10(GSEexpr)
  } else {
    if(max(GSEexpr, na.rm = T) > 100) stop("Numbers not in log-scale?")
    if(max(GSEexpr, na.rm = T) <= 0) stop("Numbers not in log-scale?")
  }
  
  GSEpDGSMs <- colnames(GSEexpr)
  
  if(isRNASeq){
    MatchIndx <- match(Annot$ID,GSEpDGSMs, nomatch = 0)
  } else {
    MatchIndx <- match(Annot$gsm,GSEpDGSMs, nomatch = 0)
  }
  
  if(any(MatchIndx==0)) stop("Sample couldn't be found in GSE Object")
  
  GSEexpr <- GSEexpr[,MatchIndx]
  
  if(Annot$Curated_Control[1]!="C") stop("expecting to have control at first")
  if(max(which(Annot$Curated_Control=="C")) >=
     min(which(Annot$Curated_Control=="T")))
    stop("expecting to have a cluster of control samples at the start,",
         "followed by cluster(s) of experiment replicates")
  
  colnames(GSEexpr) <- make.names(paste(Annot$Curated_GSE_, Annot$Curated_group,
                                        Annot$Curated_Treatment, Annot$Curated_Time,
                                        Annot$Curated_Stress, sep = ".."))
  ControlIndx <- which(Annot$Curated_Control == "C")
  return(GSEexpr[,-ControlIndx,drop=FALSE] - 
           apply(GSEexpr[,ControlIndx,drop=FALSE],1,mean))
}
